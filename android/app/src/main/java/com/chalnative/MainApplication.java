package com.chalnative;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.BV.LinearGradient.LinearGradientPackage;
import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;
import com.burnweb.rnwebview.RNWebViewPackage;
import com.psykar.cookiemanager.CookieManagerPackage;
import com.idehub.Billing.InAppBillingBridgePackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new LinearGradientPackage(),
            new RCTSplashScreenPackage(),
            new RNWebViewPackage(),
            new CookieManagerPackage(),
            new InAppBillingBridgePackage("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApjWnDsz/4KNNUUYKzWysODewGZJ6XmPcmn92v8ZIbSbf134x2A5l9bC6dctTQOPaDYZumGBrkNH3FmJqH2Fl7M1nLTFEEeHzqiUr9r8XCbs/OzV92N3RXrTltPFTF+mDO84+3a+ba26rZR0YVgsRu1RZuih5tOQ/96E4ftt0GJGvf6Ld+rTb3HRKuIV3WPYqHeNGamTtYnyiCh8pIehveH6GrkV59utMQaxmNmw/EtqVy4k7cmj0pHludn4V5sbVK0GQmrSM8W2jm7dtjdme5AD6kgyz3Vv2xmvA4CL4FNCDJ9R3XKiP6fyV0E460Mw11I5CqfwbBoGFKClU5DMCwwIDAQAB"),
            new VectorIconsPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
