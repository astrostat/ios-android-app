import React, { Component } from "react";
import { Platform, View, Text, TouchableOpacity, BackHandler, Alert } from "react-native";
import { Header } from './components/common';
import CookieManager from 'react-native-cookies';
import Icon from "react-native-vector-icons/Ionicons";
import App from "./App";

export default class Option extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loggedIn: true
    };
  }

  componentWillMount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
  }

  logoutButtonClick() {
    Alert.alert(
      '알림',
      '정말 로그아웃 하시겠어요 ?',
      [
        {text: '네', onPress: () => this.logout()},
        {text: '아니요', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      ],
      { cancelable: true }
    )
    return true;
  }

  logout () {
    CookieManager.clearAll()
      .then((err, res) => {
        console.log(err);
        console.log(res);
      }
    );

    this.setState({
      loggedIn: false,
    });
  }

  render() {
    const { container, menuIcon } = styles;
    const { navigation } = this.props;

    if (this.state.loggedIn) {
      return (
        <View style={styles.container}>
          <Header
            headerText='앱 설정'
            onPress={() => navigation.goBack()}
          />
          <View>
            <TouchableOpacity style={menuIcon} onPress={() => this.logoutButtonClick()}>
              <Text>로그아웃  </Text><Icon name="ios-log-out-outline" size={15} color="#282828" />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    else {
      return (
        <App/>
      );
    }
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  },
  menuIcon: {
    height: 55,
    width: 200,
    paddingRight: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  }
};
