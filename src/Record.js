import React, { Component } from "react";
import { Platform, StyleSheet, View, WebView } from "react-native";
import WebviewOS from './components/common/WebviewOS';
import * as CHAL_URL from "./config/chalURL";

export default class Profile extends Component {

  componentWillUnmount() {
    this.props.navigation.state.params.updateCoin();
    this.props.navigation.state.params.checkRecordRequest();
  }

  render() {
    const { container } = styles;
    const { navigation } = this.props;

    return (
        <WebviewOS
          onPress={() => navigation.goBack()}
          headerText='인터뷰 신청'
          targetURL={CHAL_URL.RECORD_URL}
        />
    );
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  }
};
