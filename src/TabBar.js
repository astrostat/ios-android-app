import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

class TabBar extends Component {
  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.tabCont}>
        <TouchableOpacity
          style={styles.tabNav}
          onPress={() => navigate("Home")}
        >
          <Icon name="ios-home-outline" size={30} color="#282828" />
          <Text style={styles.tabText}>홈</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabNav}
          onPress={() => navigate("Friend")}
        >
          <Icon name="ios-people-outline" size={30} color="#282828" />
          <Text style={styles.tabText}>친구</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabNav}
          onPress={() => navigate("Chat")}
        >
          <Icon name="ios-chatbubbles-outline" size={30} color="#282828" />
          <Text style={styles.tabText}>채팅</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabNav}
          onPress={() => navigate("Profile")}
        >
          <Icon name="ios-images-outline" size={30} color="#282828" />
          <Text style={styles.tabText}>프로필</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabNav}
          onPress={() => navigate("Payment")}
        >
          <Icon name="ios-cart-outline" size={30} color="#282828" />
          <Text style={styles.tabText}>스토어</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabCont: {
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
    borderTopColor: "#A9A9A9",
    borderTopWidth: 0.5
  },
  tabNav: {
    height: 50,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF"
  },
  tabText: {
    fontSize: 10,
    color: "#282828"
  }
});

export default TabBar;
