import React, { Component } from "react";
import { Platform, View, Text } from "react-native";
import CookieManager from 'react-native-cookies';
import IosInApp from './IosInApp';
import AndroidInApp from "./AndroidInApp";
import { Header } from './components/common';
import axios from 'axios';
import * as CHAL_URL from "./config/chalURL";

export default class Payment extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coin: 0
    };
  }

  componentWillMount() {
    CookieManager.get(CHAL_URL.HOME_URL)
    .then((res) => {
      console.log('CookieManager.get =>', res); // => 'user_session=abcdefg; path=/;'
      axios.get(CHAL_URL.COIN_URL, {
        headers: {res}
      })
      .then((response) => {
        this.setState({
          coin: response.data.coin
        });
        console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
    });
  }

  setCoin(response) {
    this.setState({
      coin: response.data.coin
    });
    this.props.navigation.state.params.updateCoin();
  }

  renderBasedOnOS() {
    if (Platform.OS === "ios") {
      return <IosInApp />;
    } else {
      return (
        <AndroidInApp
          setCoin={(response) => this.setCoin(response)}
        />
      )
    }
  }

  render() {
    const { container } = styles;
    const { navigation } = this.props;

    return (
      <View style={styles.container}>
        <Header
          headerText='스토어'
          onPress={() => navigation.goBack()}
        />
        {this.renderBasedOnOS()}
        <Text>
          {this.state.coin}
        </Text>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  }
};
