import React, { Component } from "react";
import { Platform, StyleSheet, View, WebView, BackHandler, Alert, TouchableOpacity, Text } from "react-native";
import WebViewAndroid from 'react-native-webview-android';
import CookieManager from 'react-native-cookies';
import TabBar from "./TabBar";
import { MainHeader } from './components/common';
import Icon from "react-native-vector-icons/Ionicons";
import axios from 'axios';
import * as CHAL_URL from "./config/chalURL";


export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coin: 0,
      recordRequest: true,
      targetURL: CHAL_URL.HOME_URL,
    };
  }

  componentWillMount() {
    this.checkRecordRequest();
    this.updateCoin();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  checkRecordRequest() {
    CookieManager.get(CHAL_URL.HOME_URL)
    .then((res) => {
      console.log('CookieManager.get =>', res); // => 'user_session=abcdefg; path=/;'
      axios.get(CHAL_URL.USER_INFO_URL, {
        headers: {res}
      })
      .then((response) => {
        console.log(response.data);
        if (response.data.data.attributes.record_request == false) {
          this.setState({
            recordRequest: false
          });
        } else {
          this.setState({
            recordRequest: true
          });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    });
  }

  updateCoin() {
    CookieManager.get(CHAL_URL.HOME_URL)
    .then((res) => {
      console.log('CookieManager.get =>', res); // => 'user_session=abcdefg; path=/;'
      axios.get(CHAL_URL.COIN_URL, {
        headers: {res}
      })
      .then((response) => {
        this.setState({
          coin: response.data.coin
        });
        console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
    });
  }

  updateURL(URL) {
    this.setState({
      targetURL: URL
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    Alert.alert(
      '알림',
      '앱을 종료 하시겠어요 ?',
      [
        {text: '네', onPress: () => BackHandler.exitApp()},
        {text: '아니요', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      ],
      { cancelable: true }
    )
    return true;
  }

  onNavigationStateChange (navState) {
    const { navigate } = this.props.navigation;
    console.log('navState =>', navState);

    if (navState.url == CHAL_URL.PAYMENT_URL && navState.loading == true) {

      Alert.alert(
        '망고가 부족합니다.',
        '망고를 충전하러 가시겠어요 ?',
        [
          {text: '아니요', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: '네', onPress: () => navigate("Payment", { item: null })},
        ],
        { cancelable: true }
      )
      return true;
    } else if (navState.url.split('?')[0] == CHAL_URL.EXPENSE_URL && navState.loading == true) {
      this.updateCoin();
    } else if (navState.url.split('?')[0] == CHAL_URL.CHAT_URL && navState.loading == true) {
      navigate("Chat", {updateURL: (URL) => this.updateURL(URL), id: navState.url.split('?')[1].split('=')[1] });
    }
  }

  renderWebviewBasedOnOS() {
    if (Platform.OS === "ios") {
      return (
        <WebView
          source={{ uri: this.state.targetURL }}
          style={{ flex: 1 }}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        />
      );
    } else {
      return (
        <WebViewAndroid
          source={{ uri: this.state.targetURL }}
          style={{ flex: 1 }}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        />
      );
    }
  }

  renderRecorder() {
    if (this.state.recordRequest == false) {
      return(
        <TouchableOpacity style={styles.menuIcon} onPress={() => this.props.navigation.navigate('Record', {updateCoin: () => this.updateCoin(), checkRecordRequest: () => this.checkRecordRequest()})}>
          <Icon name="ios-mic-outline" size={30} color="#aa4444" />
        </TouchableOpacity>
      )
    }
  }

  render() {
    const { navigation } = this.props;
    const { container, menuIcon, pointIcon, coinText } = styles;

    return (
      <View style={container}>
        <MainHeader
          onPressMenu={() => navigation.navigate('DrawerOpen')}
        >
          <TouchableOpacity style={pointIcon} onPress={() => navigation.navigate('Payment', {updateCoin: () => this.updateCoin()})}>
            <Text style={coinText}>
              {this.state.coin} 개
            </Text>
          </TouchableOpacity>
        </MainHeader>
        {this.renderWebviewBasedOnOS()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  },
  menuIcon: {
    height: 55,
    width: 55,
    justifyContent: 'center',
    alignItems: 'center'
  },
  pointIcon: {
    height: 55,
    width: 55,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  coinText: {
    fontWeight: '500',
    fontFamily: 'NotoSans',
    color: '#ffffff',
  }
});
