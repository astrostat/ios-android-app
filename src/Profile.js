import React, { Component } from "react";
import { Platform, StyleSheet, View, WebView } from "react-native";
import WebviewOS from './components/common/WebviewOS';
import * as CHAL_URL from "./config/chalURL";

export default class Profile extends Component {

  render() {
    const { container } = styles;
    const { navigation } = this.props;

    return (
      <View style={container}>
        <WebviewOS
          onPress={() => navigation.goBack()}
          headerText='내 프로필'
          targetURL={CHAL_URL.PROFILE_URL}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  }
};
