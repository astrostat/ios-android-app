import React, { Component } from "react";
import { StatusBar,Platform, View, Text, WebView, StyleSheet, BackHandler, Alert, Image } from 'react-native';
import SplashScreen from 'react-native-smart-splash-screen'
import WebViewAndroid from 'react-native-webview-android';
import CookieManager from 'react-native-cookies';
import RootNavigator from "./config/StackNavigator";
import * as CHAL_URL from "./config/chalURL";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      loadedCookie: false
    };
  }

  componentWillMount() {
    CookieManager.get(CHAL_URL.HOME_URL)
    .then((res) => {
      console.log('CookieManager.get =>', res); // => 'user_session=abcdefg; path=/;'
    });

    CookieManager.get(CHAL_URL.HOME_URL)
    .then((err, cookie) => {
      let isAuthenticated;

      if (cookie && cookie.hasOwnProperty('remember_me')) {
        isAuthenticated = true;
      } else {
        isAuthenticated = false;
      }

      this.setState({
        loggedIn: isAuthenticated,
        loadedCookie: true
      });
    });

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentDidMount() {
    SplashScreen.close({
       animationType: SplashScreen.animationType.scale,
       duration: 850,
       delay: 500,
    })
  }

  handleBackButtonClick() {
    Alert.alert(
      '알림',
      '앱을 종료 하시겠어요 ?',
      [
        {text: '네', onPress: () => BackHandler.exitApp()},
        {text: '아니요', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      ],
      { cancelable: true }
    )
    return true;
  }


  onNavigationStateChange (navState) {
    if (navState.url == CHAL_URL.HOME_URL) {
      this.setState({
        loggedIn: true,
      });
    }
  }

  renderWebviewBasedOnOS() {
    if (Platform.OS === "ios") {
      return (
        <WebView
          source={{ uri: CHAL_URL.LANDING_URL }}
          style={{ flex: 1 }}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        />
      );
    } else {
      return (
          <WebViewAndroid
            source={{ uri: CHAL_URL.LANDING_URL }}
            style={{ flex: 1 }}
            onNavigationStateChange={this.onNavigationStateChange.bind(this)}
          />
      );
    }
  }

  render() {
    if (this.state.loadedCookie) {
      if (this.state.loggedIn) {
        return (
          <View style={styles.container}>
            <StatusBar
              backgroundColor="#ff4252"
              barStyle="light-content"
            />
            <RootNavigator />
          </View>
        )
      }
      else {
        return (
          <View style={styles.container}>
            {this.renderWebviewBasedOnOS()}
          </View>
        );
      }
    }
    else {
      return (
        <View></View>
      );
    }
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: null,
  },
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0,
  }
});

export default App;
