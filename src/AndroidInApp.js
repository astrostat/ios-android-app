import React, { Component } from "react";
import { AppRegistry, StyleSheet, Text, View } from "react-native";
import CookieManager from 'react-native-cookies';
import { Button, ProductUnit, ProductLine } from './components/common';
import InAppBilling from "react-native-billing";
import axios from 'axios';
import * as CHAL_URL from "./config/chalURL";

export default class AndroidInApp extends Component {
  constructor(props) {
    super(props);
  }

  verifyToServer(details, url) {
    CookieManager.get(CHAL_URL.HOME_URL)
    .then((res) => {
      console.log('Start communicate with server CookieManager.get =>', res); // => 'user_session=abcdefg; path=/;'
      axios.post(url, {
        headers: {res},
        data: {
          device_os: 'android',
          productId: details.productId,
          orderId: details.orderId,
          purchaseToken: details.purchaseToken,
          purchaseTime: details.purchaseTime,
          purchaseState: details.purchaseState,
          receiptSignature: details.receiptSignature,
          receiptData: details.receiptData,
          developerPayload: details.developerPayload
        },
      }).then((response) => {
        console.log('ServerResponse =>', response);
        if (response.status == 200) { //서버에서 승인이면 200 리턴
          console.log('ServerResponse == 200');
          this.props.setCoin(response);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    });
  }

  callInAppProduct(productId) {
    InAppBilling.open()
      .then(() => InAppBilling.purchase(productId))
      .then((details) => {
        this.verifyToServer(details, CHAL_URL.VERIFY_PRODUCT_URL)
        return InAppBilling.getProductDetails(productId);
      })
      .then(productDetails => {
        this.setState({
          productDetailsText: productDetails.title
        });
        InAppBilling.consumePurchase(productId);
        return InAppBilling.close();
      })
      .catch(error => {
        return InAppBilling.close();
      });
  }

  callInAppSubscription() {
    InAppBilling.open()
      .then(() => InAppBilling.subscribe("com.chalnative.inapp.60_sub_mango"))
      .then((details) => {
        this.verifyToServer(details, CHAL_URL.VERIFY_SUBSCRIPTION_URL)
        return InAppBilling.getSubscriptionDetails("com.chalnative.inapp.60_sub_mango");
      })
      .then(productDetails => {
        this.setState({
          productDetailsText: productDetails.title
        });
        return InAppBilling.close();
      })
      .catch(error => {
        console.log('error while purchasing =>', error);
        return InAppBilling.close();
      });
  }

  callTestProduct(productId) {
    InAppBilling.open()
      .then(() => InAppBilling.purchase(productId))
      .then((details) => {
        console.log('product details before server notify');
        this.verifyToServer(details, CHAL_URL.VERIFY_PRODUCT_URL)
        return InAppBilling.getProductDetails(productId);
      })
      .then(productDetails => {
        console.log('product details after purchase');
        this.setState({
          productDetailsText: productDetails.title
        });
        InAppBilling.consumePurchase(productId);
        return InAppBilling.close();
      })
      .catch(error => {
        console.log('error while purchasing =>', error);
        return InAppBilling.close();
      });
  }

  render() {
    const { container, headerContainer, adText, headerText } = styles;

    return (
      <View style={container}>
        <View style={headerContainer}>
          <Text style={adText}>
            나의 인연을 만날 수 있는 망고를 구입하세요
          </Text>
          <Text style={headerText}>
            포인트
          </Text>
        </View>
        <ProductLine>
          <ProductUnit
            onPress={() => this.callInAppProduct('com.chalnative.inapp.10_mango')}
            priceText='10개'
          />
          <ProductUnit
            onPress={() => this.callInAppProduct('com.chalnative.inapp.30_mango')}
            priceText='30개'
          />
        </ProductLine>
        <ProductLine>
          <ProductUnit
            onPress={() => this.callInAppProduct('com.chalnative.inapp.50_mango')}
            priceText='50개'
          />
          <ProductUnit
            onPress={() => this.callInAppProduct('com.chalnative.inapp.100_mango')}
            priceText='100개'
          />
        </ProductLine>
        <ProductLine>
          <ProductUnit
            onPress={() => this.callInAppProduct('android.test.purchased')}
            priceText='테스트'
          />
        </ProductLine>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  headerContainer: {
    height: 60,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center'
  },
  adText: {
    color: '#5b5b5b',
    fontSize: 13,
    fontWeight: '100',
    textAlignVertical: 'center',
    fontFamily: 'NotoSans',
  },
  headerText: {
    color: '#ff4252',
    fontSize: 14,
    fontWeight: '500',
    textAlignVertical: 'center',
    fontFamily: 'NotoSans',
  }
});
