import React from 'react';
import { View } from 'react-native';

const ProductLine = ({ children }) => {
  const { container } = styles;

  return(
    <View style={container}>
      {children}
    </View>
  );
};

const styles = {
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 150,
  }
};

export { ProductLine };
