// Import libraries for making a component
import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

// Make a component
const Header = ({ onPress, headerText }) => {
    const { textStyle, viewStyle, menuIcon } = styles;

    return (
      <View style={viewStyle}>
        <TouchableOpacity style={menuIcon} onPress={onPress}>
          <Icon name="ios-arrow-round-back" size={30} color="#282828" />
        </TouchableOpacity>
        <Text style={textStyle}>{headerText}</Text>
      </View>
    );
};

const styles = {
  viewStyle: {
      backgroundColor: '#ffffff',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      height: 55,
      elevation: 2,
      position: 'relative'
  },
  textStyle: {
      fontSize: 20,
      justifyContent: 'center',
      alignItems: 'center'
  },
  menuIcon: {
    height: 55,
    width: 55,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

// Make the component available to other parts of the app
export { Header };
