// Import libraries for making a component
import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

const MainHeader = ({ onPressMenu, onPressRecord, onPressPay, pointText, children }) => {
    const { textStyle, viewStyle, menuLeft, menuCenter, menuRight, menuIcon, pointIcon, logoText, imageStyle } = styles;

    return (
      <View style={viewStyle}>
        <View style={menuLeft}>
          <TouchableOpacity style={menuIcon} onPress={onPressMenu}>
            <Icon name="ios-menu" size={35} color="#ffffff" />
          </TouchableOpacity>
        </View>
        <View style={menuCenter}>
          <Image source={require('../../assets/images/mango_image_plain.png')} style={imageStyle} />
        </View>
        <View style={menuRight}>
          {children}
        </View>
      </View>
    );
};

const styles = {
  viewStyle: {
    backgroundColor: '#ff4252',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 55,
    elevation: 1,
    position: 'relative',
  },
  textStyle: {
      fontSize: 20
  },
  menuLeft: {
    flex: 1,
    flexDirection: 'row',
    height: 55,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  menuCenter: {
    flex: 1,
    flexDirection: 'row',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center'
  },
  menuRight: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  menuIcon: {
    height: 55,
    width: 55,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoText: {
    fontFamily: "NotoSans"
  },
  imageStyle: {
    height: 55/2,
    width: (156/121)*55/2,
  }
};

// Make the component available to other parts of the app
export { MainHeader };
