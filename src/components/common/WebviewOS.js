// Import libraries for making a component
import React, { Component } from 'react';
import { Platform, WebView, View } from 'react-native';
import WebViewAndroid from 'react-native-webview-android';
import { Header } from './Header';

export default class WebviewOS extends Component {
  constructor(props) {
    super(props);
  }

  renderBasedOnOS(targetURL, onNavigationStateChange) {
    if (Platform.OS === "ios") {
      return (
          <WebView
            source={{ uri: targetURL }}
            style={{ flex: 1 }}
            onNavigationStateChange={onNavigationStateChange}
          />
      );
    } else {
      return (
          <WebViewAndroid
            source={{ uri: targetURL }}
            style={{ flex: 1 }}
            onNavigationStateChange={onNavigationStateChange}
          />
      );
    }
  }

  render() {
    const { container } = styles;
    const { headerText, onPress, targetURL, onNavigationStateChange } = this.props;

    return(
      <View style={container}>
        <Header
          headerText={headerText}
          onPress={onPress}
        />
        {this.renderBasedOnOS(targetURL, onNavigationStateChange)}
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  },
};
