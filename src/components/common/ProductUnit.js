import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

const ProductUnit = ({ onPress, priceText }) => {
  const { buttonStyle, imageStyle, textStyle } = styles;

  return(
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
        <Image source={require('../../assets/images/mango_image.png')} style={imageStyle} />
        <Text style={textStyle}>
            {priceText}
        </Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    height: 130,
    width: 130,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 75,
    borderWidth: 2,
    borderColor: '#ff4252',
    marginLeft: 15,
    marginRight: 15,
  },
  imageStyle: {
    marginTop: 10,
    height: (121/3.5),
    width: (154/3.5),
  },
  textStyle: {
    color: '#ff4252',
    fontSize: 20,
    fontWeight: '700',
    lineHeight: 30,
    textAlignVertical: 'center',
    fontFamily: 'NotoSans',
  }
};

export { ProductUnit };
