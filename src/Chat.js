import React, { Component } from "react";
import { Platform, StyleSheet, View, WebView } from "react-native";
import WebviewOS from './components/common/WebviewOS';
import * as CHAL_URL from "./config/chalURL";

export default class Chat extends Component {

  onNavigationStateChange (navState) {
    const { navigate } = this.props.navigation;

    if (navState.url.split('?')[0] == CHAL_URL.CHAT_PROFILE_URL && navState.loading == true) {
      navigate("Friend", { friendId: navState.url.split('?')[1].split('=')[1] });
    }
  }

  componentWillUnmount() {
    console.log('unmounted');
    this.props.navigation.state.params.updateURL(CHAL_URL.CONVERSATION_URL);
  }

  render() {
    const { container } = styles;
    const { navigation } = this.props;

    return (
      <View style={container}>
        <WebviewOS
          onPress={() => navigation.goBack()}
          headerText='채팅방'
          targetURL={CHAL_URL.CONVERSATION_URL + '/' + navigation.state.params.id}
          onNavigationStateChange={(navState) => this.onNavigationStateChange(navState)}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  }
};
