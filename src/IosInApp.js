import React, { Component } from "react";
import { Platform, Text, View } from "react-native";
import { Button, ProductLine } from './components/common';

export default class IosInApp extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {

  }

  render() {
    const { container } = styles;

    return (
      <View>
        <Text>
          iOS in app purchase logic
        </Text>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 0
  }
};
