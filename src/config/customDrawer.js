// Import libraries for making a component
import React, { Component } from 'react';
import { Platform, Webview, View, Text, TouchableOpacity, Image } from 'react-native';
import { DrawerItems } from "react-navigation";
import Icon from "react-native-vector-icons/Ionicons";
import CookieManager from 'react-native-cookies';
import LinearGradient from 'react-native-linear-gradient';
import App from "../App.js";
import axios from 'axios';
import * as CHAL_URL from "./chalURL";

export default class customDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userImage: './assets/images/missing.png',
    }
  }

  componentWillMount() {
    CookieManager.get(CHAL_URL.HOME_URL)
    .then((res) => {
      console.log('CookieManager.get =>', res); // => 'user_session=abcdefg; path=/;'
      axios.get(CHAL_URL.PROFILE_IMAGE_URL, {
        headers: {res}
      })
      .then((response) => {
        console.log('image response ->', response.data);
        this.setState({
          userImage: response.data.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
    });
  }

  render() {
    const { container, drawerTop, drawerBottom, menuBottom, menuIcon, logoContainer, logoStyle, profileContainer, imageContainer, imageStyle, settingText } = styles;
    const { navigation } = this.props;

    return(
      <LinearGradient colors={['#ff414f', '#ff658e', '#ff8f6a']} style={container}>
        <View style={drawerTop}>
          <View style={logoContainer}>
            <Image
              style={logoStyle}
              source={require('../assets/images/hawaiian_logo.png')}
            />
          </View>
          <View style={profileContainer}>
            <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
              <View style={imageContainer}>
                <Image
                  style={imageStyle}
                  source={{uri: this.state.userImage}}
                />
              </View>
            </TouchableOpacity>
          </View>
          <DrawerItems {...this.props} />
        </View>
        <View style={drawerBottom}>
          <TouchableOpacity style={menuIcon} onPress={() => navigation.navigate("Option")}>
            <Text style={settingText}>앱 설정  </Text><Icon name="ios-settings-outline" size={15} color="#ffffff" />
          </TouchableOpacity>
        </View>
      </LinearGradient>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'space-between',
    marginTop: Platform.OS === "ios" ? 20 : 0,
    backgroundColor: "#ff4252",
  },
  drawerTop: {
    flex: 1,
  },
  drawerBottom: {
    height: 55,
  },
  menuIcon: {
    height: 55,
    width: 200,
    paddingRight: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  logoContainer: {
    height: 120,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoStyle: {
    height: 100*(299/539),
    width: 100,
  },
  profileContainer: {
    height: 120,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer: {
    height: 100,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#ffffff',
  },
  imageStyle: {
    height: 94,
    width: 94,
    borderRadius: 47,
  },
  settingText: {
    color: '#ffffff'
  }
};
