import { StackNavigator } from "react-navigation";

import Home from "../Home";
import Friend from "../Friend";
import Chat from "../Chat";
import Profile from "../Profile";
import Payment from "../Payment";
import Record from "../Record";

let MyTransition = (index, position) => {
  const inputRange = [index - 1, index, index + 1];
  const opacity = position.interpolate({
    inputRange,
    outputRange: [1, 1, 1]
  });

  const scaleY = position.interpolate({
    inputRange,
    outputRange: [1, 1, 1]
  });

  return {
    opacity,
    transform: [{ scaleY }]
  };
};

let TransitionConfiguration = () => {
  return {
    // Define scene interpolation, eq. custom transition
    screenInterpolator: sceneProps => {
      const { position, scene } = sceneProps;
      const { index } = scene;

      return MyTransition(index, position);
    }
  };
};

const RootNavigator = StackNavigator(
  {
    // screen navigation 설정
    Home: { screen: Home },
    //Friend: { screen: Friend },
    //Chat: { screen: Chat },
    //Profile: { screen: Profile },
    Payment: { screen: Payment },
    //Record: { screen: Record }
  },
  {
    // navigation option 설정
    headerMode: "none",
    //transitionConfig: TransitionConfiguration
  }
);

export default RootNavigator;
