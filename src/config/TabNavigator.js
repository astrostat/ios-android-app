import React from "react";
import { TabNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/Ionicons";

import Home from "../Home";
import Friend from "../Friend";
import Chat from "../Chat";
import Profile from "../Profile";
import Payment from "../Payment";

const RootNavigator = TabNavigator(
  {
    // screen navigation 설정
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: '홈',
        tabBarIcon: ({ tintColor }) => <Icon name="ios-home-outline" size={30} color={tintColor} />
      },
    },
    Friend: {
      screen: Friend,
      navigationOptions: {
        tabBarLabel: '친구',
        tabBarIcon: ({ tintColor }) => <Icon name="ios-people-outline" size={30} color={tintColor} />
      },
    },
    Chat: {
      screen: Chat,
      navigationOptions: {
        tabBarLabel: '채팅',
        tabBarIcon: ({ tintColor }) => <Icon name="ios-chatbubbles-outline" size={30} color={tintColor} />
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarLabel: '프로필',
        tabBarIcon: ({ tintColor }) => <Icon name="ios-images-outline" size={30} color={tintColor} />
      },
    },
    Payment: {
      screen: Payment,
      navigationOptions: {
        tabBarLabel: '결재',
        tabBarIcon: ({ tintColor }) => <Icon name="ios-card-outline" size={30} color={tintColor} />
      },
    }
  },
  {
    // navigation option 설정
    headerMode: "none"
  }
);

export default RootNavigator;
