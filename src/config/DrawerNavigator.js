import React from "react";
import { View, Text } from 'react-native';
import { DrawerNavigator, DrawerItems } from "react-navigation";
import Icon from "react-native-vector-icons/Ionicons";

import Home from "../Home";
import Payment from "../Payment";
import Profile from "../Profile";
import Record from "../Record";
import customDrawer from "./customDrawer";

const DrawingNavigator = DrawerNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        drawerLabel: '홈으로가기',
      }
    }
  },
  {
    drawerWidth: 200,
    drawerPosition: 'left',
    contentComponent: customDrawer
  }
);

export default DrawingNavigator;
