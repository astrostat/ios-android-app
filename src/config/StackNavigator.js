import React from "react";
import { StackNavigator } from "react-navigation";
import DrawingNavigator from "./DrawerNavigator";
import Icon from "react-native-vector-icons/Ionicons";
import Option from "../Option";
import Payment from "../Payment";
import Profile from "../Profile";
import Record from "../Record";
import Chat from "../Chat";
import Friend from "../Friend";

const RootNavigator = StackNavigator(
  {
    Main: {
      screen: DrawingNavigator,
    },
    Payment: {
      screen: Payment,
    },
    Profile: {
      screen: Profile,
    },
    Record: {
      screen: Record,
    },
    Chat: {
      screen: Chat,
    },
    Friend: {
      screen: Friend,
    },
    Option: {
      screen: Option,
    },
  },
  {
    // navigation option 설정
    headerMode: "none"
  }
);

export default RootNavigator;
