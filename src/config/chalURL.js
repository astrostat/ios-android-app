//export const DOMAIN = "http://172.30.24.207:3000";
//export const DOMAIN = "http://52.78.246.210";
export const DOMAIN = "http://app.hcouple.xyz";
export const API = DOMAIN + "/api";

export const HOME_URL = DOMAIN + "/";
export const FRIEND_URL = DOMAIN + "/friends";
export const PROFILE_URL = DOMAIN + "/profile";
export const PROFRIEND_URL = DOMAIN + "/profile/friend";
export const RECORD_URL = DOMAIN + "/record";
export const LOGIN_URL = DOMAIN + "/users/sign_in";
export const LANDING_URL = DOMAIN + "/app/index";
export const CONVERSATION_URL = DOMAIN + "/conversations"

export const COIN_URL = API + "/bank/wallet";
export const USER_INFO_URL = API + "/user/information";
export const PROFILE_IMAGE_URL = API + "/user/profile_image";
export const PAYMENT_URL = API + "/purchase/call";
export const EXPENSE_URL = API + "/purchase/expense";
export const VERIFY_PRODUCT_URL = API + "/purchase/verify_product";
export const VERIFY_SUBSCRIPTION_URL = API + "/purchase/verify_subscripaftion";
export const CHAT_URL = API + "/chat/call";
export const CHAT_PROFILE_URL = API + "/chat/friend";
